   # Avoid running unnecessary services on the Sidekiq server
   gitaly['enable'] = false
   postgresql['enable'] = false
   redis['enable'] = false
   nginx['enable'] = false
   puma['enable'] = false
   prometheus['enable'] = false
   alertmanager['enable'] = false
   grafana['enable'] = false
   gitlab_exporter['enable'] = false
   gitlab_kas['enable'] = false

   # External URL
   ## This should match the URL of the external load balancer
   external_url 'http://{YOUR_EIP}'
   
   # Redis
   redis['master_name'] = 'gitlab-redis'

   ## The same password for Redis authentication you set up for the master node.
   redis['master_password'] = '{YOUR_PASSWORD}'

   ## A list of sentinels with `host` and `port`
   gitlab_rails['redis_sentinels'] = [
      {'host' => '172.31.9.196', 'port' => 26379},
      {'host' => '172.31.7.156', 'port' => 26379},
      {'host' => '172.31.7.248', 'port' => 26379},
   ]

   # Gitaly Cluster
   ## git_data_dirs get configured for the Praefect virtual storage
   ## Address is Internal Load Balancer for Praefect
   ## Token is praefect_external_token
   git_data_dirs({
     "default" => {
       "gitaly_address" => "tcp://172.31.47.178:2305", # internal load balancer IP
       "gitaly_token" => '{YOUR_PASSWORD}'
     }
   })

   # PostgreSQL
   gitlab_rails['db_host'] = '172.31.47.178' # internal load balancer IP
   gitlab_rails['db_port'] = 6432
   gitlab_rails['db_password'] = '{YOUR_PASSWORD}'
   gitlab_rails['db_load_balancing'] = { 'hosts' => ['172.31.9.196', '172.31.7.156', '172.31.7.248'] } # PostgreSQL IPs

   ## Prevent database migrations from running on upgrade automatically
   gitlab_rails['auto_migrate'] = false

   # Sidekiq
   sidekiq['enable'] = true
   sidekiq['listen_address'] = "0.0.0.0"

   ## Set number of Sidekiq queue processes to the same number as available CPUs
   sidekiq['queue_groups'] = ['*'] * 2

   ## Set number of Sidekiq threads per queue process to the recommend number of 20
   sidekiq['max_concurrency'] = 20

   # Monitoring
   consul['enable'] = true
   consul['monitoring_service_discovery'] =  true

   consul['configuration'] = {
      retry_join: %w(172.31.9.196 172.31.7.156 172.31.7.248),
   }

   ## Set the network addresses that the exporters will listen on
   node_exporter['listen_address'] = '0.0.0.0:9100'

   ## Add the monitoring node's IP address to the monitoring whitelist
   gitlab_rails['monitoring_whitelist'] = ['172.31.0.0/16', '127.0.0.0/8']
   gitlab_rails['prometheus_address'] = '172.31.44.179:9090'

   # Object Storage
   ## This is an example for configuring Object Storage on GCP
   ## Replace this config with your chosen Object Storage provider as desired
   gitlab_rails['object_store']['enabled'] = true
   gitlab_rails['object_store']['connection'] = {
     'provider' => 'AWS',
     'aws_access_key_id' => '{YOUR_AWS_ACESS_KEY}',
     'aws_secret_access_key' => '{YOUR_AWS_SECRETS_ACCESS_KEY}',
     'region' => 'eu-west-1'
   }

   
gitlab_rails['object_store']['objects']['artifacts']['bucket'] = 'sporchia-test-artifacts'
gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = 'sporchia-test-mr-diffs'
gitlab_rails['object_store']['objects']['lfs']['bucket'] = 'sporchia-test-lfs'
gitlab_rails['object_store']['objects']['uploads']['bucket'] = 'sporchia-test-uploads'
gitlab_rails['object_store']['objects']['packages']['bucket'] = 'sporchia-test-packages'
gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = 'sporchia-test-dependency-proxy'
gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = 'sporchia-test-terraform-state'
gitlab_rails['object_store']['objects']['pages']['bucket'] = 'sporchia-test-pages'
#    gitlab_rails['backup_upload_connection'] = {
#      'provider' => 'Google',
#      'google_project' => '<gcp-project-name>',
#      'google_json_key_location' => '<path-to-gcp-service-account-key>'
#    }
#    gitlab_rails['backup_upload_remote_directory'] = "<gcp-backups-state-bucket-name>"

## Disable components that will not be on the GitLab application server
roles(['application_role'])
gitaly['enable'] = false
nginx['enable'] = true

## PostgreSQL connection details
# Disable PostgreSQL on the application node
postgresql['enable'] = false

# Prevent database migrations from running on upgrade automatically
gitlab_rails['auto_migrate'] = false

# Set the network addresses that the exporters used for monitoring will listen on
gitlab_workhorse['prometheus_listen_addr'] = '0.0.0.0:9229'
sidekiq['listen_address'] = "0.0.0.0"
puma['listen'] = '0.0.0.0'

# Add the monitoring node's IP address to the monitoring whitelist and allow it to
# scrape the NGINX metrics
nginx['status']['options']['allow'] = ['172.31.0.0/16', '127.0.0.0/8']
# Specify server role as 'redis_master_role' and enable Consul agent
roles(['redis_master_role', 'consul_role', 'redis_sentinel_role','patroni_role', 'pgbouncer_role'])
#roles(['patroni_role', 'pgbouncer_role'])
# IP address pointing to a local IP that the other machines can reach to.
# You can also set bind to '0.0.0.0' which listen in all interfaces.
# If you really need to bind to an external accessible IP, make
# sure you add extra firewall rules to prevent unauthorized access.
redis['bind'] = '172.31.9.196'

# Define a port so Redis can listen for TCP requests which will allow other
# machines to connect to it.
redis['port'] = 6379

# Set up password authentication for Redis (use the same password in all nodes).
redis['password'] = '{YOUR_PASSWORD}'

## Enable service discovery for Prometheus
consul['monitoring_service_discovery'] =  true

## The IPs of the Consul server nodes
## You can also use FQDNs and intermix them with IPs

# Set the network addresses that the exporters will listen on
redis_exporter['listen_address'] = '0.0.0.0:9121'
redis_exporter['flags'] = {
     'redis.addr' => 'redis://172.31.9.196:6379',
     'redis.password' => '{YOUR_PASSWORD}',
}

# Prevent database migrations from running on upgrade automatically
gitlab_rails['auto_migrate'] = false

##sentinel
## Must be the same in every sentinel node
redis['master_name'] = 'gitlab-redis'

# The same password for Redis authentication you set up for the primary node.
redis['master_password'] = '{YOUR_PASSWORD}'

# The IP of the primary Redis node.
redis['master_ip'] = '172.31.9.196'

# Define a port so Redis can listen for TCP requests which will allow other
# machines to connect to it.
redis['port'] = 6379

# Port of primary Redis server, uncomment to change to non default. Defaults
# to `6379`.
#redis['master_port'] = 6379

## Configure Sentinel
sentinel['bind'] = '172.31.9.196'

# Port that Sentinel listens on, uncomment to change to non default. Defaults
# to `26379`.
# sentinel['port'] = 26379

## Quorum must reflect the amount of voting sentinels it take to start a failover.
## Value must NOT be greater then the amount of sentinels.
##
## The quorum can be used to tune Sentinel in two ways:
## 1. If a the quorum is set to a value smaller than the majority of Sentinels
##    we deploy, we are basically making Sentinel more sensible to primary failures,
##    triggering a failover as soon as even just a minority of Sentinels is no longer
##    able to talk with the primary.
## 1. If a quorum is set to a value greater than the majority of Sentinels, we are
##    making Sentinel able to failover only when there are a very large number (larger
##    than majority) of well connected Sentinels which agree about the primary being down.s
sentinel['quorum'] = 2

## Consider unresponsive server down after x amount of ms.
# sentinel['down_after_milliseconds'] = 10000

## Specifies the failover timeout in milliseconds. It is used in many ways:
##
## - The time needed to re-start a failover after a previous failover was
##   already tried against the same primary by a given Sentinel, is two
##   times the failover timeout.
##
## - The time needed for a replica replicating to a wrong primary according
##   to a Sentinel current configuration, to be forced to replicate
##   with the right primary, is exactly the failover timeout (counting since
##   the moment a Sentinel detected the misconfiguration).
##
## - The time needed to cancel a failover that is already in progress but
##   did not produced any configuration change (REPLICAOF NO ONE yet not
##   acknowledged by the promoted replica).
##
## - The maximum time a failover in progress waits for all the replica to be
##   reconfigured as replicas of the new primary. However even after this time
##   the replicas will be reconfigured by the Sentinels anyway, but not with
##   the exact parallel-syncs progression as specified.
# sentinel['failover_timeout'] = 60000

## Enable service discovery for Prometheus
consul['monitoring_service_discovery'] =  true

## The IPs of the Consul server nodes
## You can also use FQDNs and intermix them with IPs
consul['configuration'] = {
   server: true,
   retry_join: %w(172.31.9.196 172.31.7.156 172.31.7.248),
}

# Set the network addresses that the exporters will listen on
node_exporter['listen_address'] = '0.0.0.0:9100'
redis_exporter['listen_address'] = '0.0.0.0:9121'

# Prevent database migrations from running on upgrade automatically
gitlab_rails['auto_migrate'] = false

# PostgreSQL configuration
postgresql['listen_address'] = '0.0.0.0'

# Sets `max_replication_slots` to double the number of database nodes.
# Patroni uses one extra slot per node when initiating the replication.
patroni['postgresql']['max_replication_slots'] = 6

# Set `max_wal_senders` to one more than the number of replication slots in the cluster.
# This is used to prevent replication from using up all of the
# available database connections.
patroni['postgresql']['max_wal_senders'] = 7

# Incoming recommended value for max connections is 500. See https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/5691.
patroni['postgresql']['max_connections'] = 500

# Prevent database migrations from running on upgrade automatically
gitlab_rails['auto_migrate'] = false

# Configure the Consul agent
consul['services'] = %w(postgresql)
## Enable service discovery for Prometheus
consul['monitoring_service_discovery'] =  true


# START user configuration
# Please set the real values as explained in Required Information section
#
# Replace PGBOUNCER_PASSWORD_HASH with a generated md5 value
postgresql['pgbouncer_user_password'] = 'YOUR_PGBOUNCER_PSW_HASH'
# Replace POSTGRESQL_REPLICATION_PASSWORD_HASH with a generated md5 value
postgresql['sql_replication_password'] = 'YOUR_PSQL_REP_PSW_HASH'
# Replace POSTGRESQL_PASSWORD_HASH with a generated md5 value
postgresql['sql_user_password'] = 'YOUR_PSQL_PSW_HASH'

# Set up basic authentication for the Patroni API (use the same username/password in all nodes).
patroni['username'] = 'gitlab'
patroni['password'] = '{YOUR_PASSWORD}'

# Replace 10.6.0.0/24 with Network Addresses for your other patroni nodes
patroni['allowlist'] = %w(172.31.0.0/16 127.0.0.1/32)

# Replace 10.6.0.0/24 with Network Address
postgresql['trust_auth_cidr_addresses'] = %w(172.31.0.0/16 127.0.0.1/32)

# Local PgBouncer service for Database Load Balancing
pgbouncer['databases'] = {
   gitlabhq_production: {
      host: "127.0.0.1",
      user: "pgbouncer",
      password: 'YOUR_PGBOUNCER_PSW_HASH'
   }
}

# Set the network addresses that the exporters will listen on for monitoring
postgres_exporter['listen_address'] = '0.0.0.0:9187'

# Set the network addresses that the exporters will listen on for monitoring
node_exporter['listen_address'] = '0.0.0.0:9100'
postgres_exporter['listen_address'] = '0.0.0.0:9187'

# Configure PgBouncer
pgbouncer['admin_users'] = %w(pgbouncer gitlab-consul)
pgbouncer['users'] = {
   'gitlab-consul': {
      password: 'YOUR_CONSUL_PASSWORD_HASH'
   },
   'pgbouncer': {
      password: 'YOUR_PGBOUNCER_PSW_HASH'
   }
}
# Incoming recommended value for max db connections is 150. See https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/5691.
pgbouncer['max_db_connections'] = 150

# Configure Consul agent
consul['watchers'] = %w(postgresql)

# Enable service discovery for Prometheus
# Set the network addresses that the exporters will listen on
pgbouncer_exporter['listen_address'] = '0.0.0.0:9188'

##### PRAEFECT

# Praefect Configuration
praefect['enable'] = true

# Prevent database migrations from running on upgrade automatically
praefect['auto_migrate'] = true
gitlab_rails['auto_migrate'] = false

# Configure the Consul agent
consul['enable'] = true
## Enable service discovery for Prometheus
consul['monitoring_service_discovery'] = true

# START user configuration
# Please set the real values as explained in Required Information section
#

praefect['configuration'] = {
   # ...
   listen_addr: '0.0.0.0:2305',
   auth: {
     # ...
     #
     # Praefect External Token
     # This is needed by clients outside the cluster (like GitLab Shell) to communicate with the Praefect cluster
     token: '{YOUR_PASSWORD}',
   },
   # Praefect Database Settings
   database: {
     # ...
     host: '172.31.44.179',
     port: 5432,
     user: 'praefect',
     dbname: 'praefect_production',
     # `no_proxy` settings must always be a direct connection for caching
     # i
     sslmode: 'disable',
     session_pooled: {
        # ...
        host: '172.31.44.179',
        port: 5432,
        dbname: 'praefect_production',
        user: 'praefect',
        sslmode: 'disable',
     },
   },
   # Praefect Virtual Storage config
   # Name of storage hash must match storage name in git_data_dirs on GitLab
   # server ('praefect') and in gitaly['configuration'][:storage] on Gitaly nodes ('gitaly-1')
   virtual_storage: [
      {
         # ...
         name: 'default',
         node: [
            {
               storage: 'gitaly-1',
               address: 'tcp://172.31.9.196:8075',
               token: '{YOUR_PASSWORD}'
            },
            {
               storage: 'gitaly-2',
               address: 'tcp://172.31.7.156:8075',
               token: '{YOUR_PASSWORD}'
            },
            {
               storage: 'gitaly-3',
               address: 'tcp://172.31.7.248:8075',
               token: '{YOUR_PASSWORD}'
            },
         ],
      },
   ],
   # Set the network address Praefect will listen on for monitoring
   prometheus_listen_addr: '0.0.0.0:9652',
}

# Set the network address the node exporter will listen on for monitoring
node_exporter['listen_address'] = '0.0.0.0:9100'
#
# END user configuration
#

# Prevent database migrations from running on upgrade automatically
gitlab_rails['auto_migrate'] = false

# Gitaly
gitaly['enable'] = true

# Configure the gitlab-shell API callback URL. Without this, `git push` will
# fail. This can be your 'front door' GitLab URL or an internal load
# balancer.
gitlab_rails['internal_api_url'] = 'http://{YOUR_EIP}'

# Configure the Consul agent
consul['enable'] = true
## Enable service discovery for Prometheus
consul['monitoring_service_discovery'] = true

# START user configuration
# Please set the real values as explained in Required Information section
#
## The IPs of the Consul server nodes
## You can also use FQDNs and intermix them with IPs

# Set the network address that the node exporter will listen on for monitoring
node_exporter['listen_address'] = '0.0.0.0:9100'

gitaly['configuration'] = {
   # ...
   #
   # Make Gitaly accept connections on all network interfaces. You must use
   # firewalls to restrict access to this address/port.
   # Comment out following line if you only want to support TLS connections
  listen_addr: '0.0.0.0:8075',
   # Set the network address that Gitaly will listen on for monitoring
   prometheus_listen_addr: '0.0.0.0:9236',
   # Gitaly Auth Token
   # Should be the same as praefect_internal_token
   auth: {
      # ...
      token: '{YOUR_PASSWORD}',
   },
   # Gitaly Pack-objects cache
   # Recommended to be enabled for improved performance but can notably increase disk I/O
   # Refer to https://docs.gitlab.com/ee/administration/gitaly/configure_gitaly.html#pack-objects-cache for more info
   pack_objects_cache: {
      # ...
      enabled: true,
   },
   storage: [
     {
       name: 'gitaly-1',
       path: '/var/opt/gitlab/git-data',
     },
     {
       name: 'gitaly-2',
       path: '/var/opt/gitlab/git-data',
     },
     {
       name: 'gitaly-3',
       path: '/var/opt/gitlab/git-data',
     },
   ],
}


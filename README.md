# squeezed-3k-ra

PoC for a 3k HA GitLab architecture, squeezed into only 3 backend servers.
This solution hasn't been tested thoroughly and it's not meant for production workloads - use at your own risk.
It is not an approach we suggest anyone to follow, head over here for them: https://docs.gitlab.com/ee/administration/reference_architectures/
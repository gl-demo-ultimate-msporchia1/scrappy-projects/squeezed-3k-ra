variable "prefix" {
  default = "sporchia-test"
}

variable "region" {
  default = "eu-west-1"
}

variable "ssh_public_key_file" {
  default = "../../../keys/get-sporchia-test.pub"
}

variable "external_ip_allocation" {
  default = "eipalloc-0be946945d234034c"
}

module "gitlab_ref_arch_aws" {
  source = "../../modules/gitlab_ref_arch_aws"

  prefix = var.prefix
  ssh_public_key = file(var.ssh_public_key_file)

  # 2k - super restricted

  gitlab_rails_node_count = 2
  gitlab_rails_instance_type       = "m6a.xlarge"

  haproxy_external_node_count                = 1
  haproxy_external_instance_type             = "c5.large"
  haproxy_external_elastic_ip_allocation_ids = [var.external_ip_allocation]
  haproxy_internal_node_count                = 1
  haproxy_internal_instance_type             = "c5.large"

  monitor_node_count    = 1
  monitor_instance_type       = "c5.large"

  postgres_node_count = 3
  postgres_instance_type       = "m5a.4xlarge"

}

output "gitlab_ref_arch_aws" {
  value = module.gitlab_ref_arch_aws
}